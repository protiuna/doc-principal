export default {
  title: 'Gerencia de Informática',
  description: 'Sistemas de Fogade',
  base: '/', //  The default path during deployment / secondary address / base can be used/
  themeConfig: {
    logo: '/logo.png',
    nav: [{ text: 'GitLab', link: 'https://gitlab.com/protiuna/doc-principal' }],
    sidebar: [
      {
        text: 'Tutorial',   // required
        path: '/guide/',      // optional, link of the title, which should be an absolute path and must exist        
        sidebarDepth: 1,    // optional, defaults to 1 
        items: [
          { text: 'Contexto', link: '/guide/intro' },
          { text: 'Documentacion Sistema', link: '/guide/documentation_system' },
          { text: 'La Sincronización', link: '/guide/synchronization' },
          { text: 'Requerimientos', link: '/guide/requirements' },
          { text: 'la firma', link: '/guide/business' }      
        ]
      }
    ]
  }
}
