---
# https://vitepress.dev/reference/default-theme-home-page
layout: home

hero:
  name: "Gerencia de Informática"
  text: "Documentación"
  tagline: "Información Tecnológica de FOGADE" 
  image:
    src: /logo.png
    alt: Documentación Gerencia Informática 
  actions:
    - theme: brand
      text: Auxilio Financiero
      link: http://auxilio.docs.fogade.gob.ve/
    - theme: brand
      text: Bases de Datos
      link: http://basesdedatos.docs.fogade.gob.ve/
    - theme: brand
      text: Constancia y Recibo
      link: http://constancia.docs.fogade.gob.ve/
    - theme: brand
      text: Intranet
      link: http://intranetweb.docs.fogade.gob.ve/
    - theme: brand
      text: SAIRO
      link: http://sairo.docs.fogade.gob.ve 
    - theme: brand
      text: Servidores
      link: http://servidores.docs.fogade.gob.ve  
    - theme: brand
      text: SIGICOV
      link: http://sigicov.docs.fogade.gob.ve
      

features:

  - title: ¿Qué es un Auxilio Financiero?
    details: Un auxilio financiero es cuando el gobierno proporciona dinero y/o recursos a una empresa que se puede encontrar en dificultades financieras y necesita ser auxiliado. Estas acciones son para prevenir la quiebra, cierre o el incumplimiento de las obligaciones financieras por parte de la empresa.
  - title: Bases de Datos
    details: Esta base de conocimiento fue creada con la finalidad de facilitar los procesos realizados diariamente con las bases de datos existentes en la institución.
  - title: Constancia y Recibo 
    details: Sistema que permite  Facilitar al usuario final de unamanera clara y sencilla, las instrucciones necesarias que le permitan acceder de forma mucho más rápida y eficiente a sus recibos de pagos y constancia de trabajo.  
  - title: Qué es  la intranet
    details: Es una plataforma digital cuyo objetivo es asistir a los trabajadores en la generación de valor para la institución , poniendo a su disposición activos como contenidos, archivos, procesos de negocio y herramientas; facilitando la colaboración y comunicación entre las personas y los equipos.
  - title: SAIRO
    details: Sistema de Administración Integral de Riesgos Operacionales. Herramienta para abordar la inevitable incertidumbre asociada con los negocios y proyectos ya que muestra una visión global de los riesgos y oportunidades.
  - title: Servidores
    details:  Fogade a nivel Informático maneja servicios de Sistema de Información, Consultoría Informática,Gestión de Sistemas, Correo, Aportes de la Banca entre otros.
  - title: SIGICOV
    details: SIGICOV es el acrónimo de Sistema Integrado de Gestión para Entes del Sector Público. Es un sistema administrativo ERP creado en Venezuela . enfocado en la administración pública venezolana.
 
---
